﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace autoUpdate
{
    class Program
    {
        public static string process = "EliteDangerousRouteGUI.exe";
        public static string processNoExt = "EliteDangerousRouteGUI";
        static void Main(string[] args)
        {
            foreach (Process p in Process.GetProcessesByName(processNoExt))
            {
                if (!p.HasExited)
                    p.Kill();
            }
            if (Process.GetProcessesByName("VR_Game_Manager").Length > 0)
                System.Windows.Forms.MessageBox.Show("Game Manager is still running, AutoUpdate Failed.");

            string argument = string.Format("-o\"{0}\" -y", Environment.CurrentDirectory);
            ProcessStartInfo pinfo = new ProcessStartInfo("latest_update.exe", argument);
            pinfo.WorkingDirectory = Environment.CurrentDirectory;
            pinfo.WindowStyle = ProcessWindowStyle.Hidden;
            pinfo.CreateNoWindow = true;

            Process pro = Process.Start(pinfo);
            while (!pro.HasExited)
            {
                System.Threading.Thread.Sleep(500);
            }
            Process.Start(process, "1");
        }
    }
}
  