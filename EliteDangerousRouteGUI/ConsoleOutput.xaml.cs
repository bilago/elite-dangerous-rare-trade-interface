﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Threading;

namespace EliteDangerousRouteGUI
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class ConsoleOutput : Window
    {
        public ConsoleOutput(bool isTopmost)
        {
            InitializeComponent();
            this.Topmost = isTopmost;
        }
        
        public void DoEvents()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;
            return null;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            show_routes.IsEnabled = false;
            //Thread thread = new Thread(executeRuby);
            //thread.IsBackground = true;
            //thread.Start();
            //while (thread.IsAlive)
            //{
            //    DoEvents();
            //    Thread.Sleep(100);
            //}
            executeRuby();
            show_routes.IsEnabled = true;
        }

        void executeRuby()
        {
            stopRuby.Visibility = System.Windows.Visibility.Visible;
            show_routes.IsEnabled = false;
            console_output.Text = "Please wait while this window populates...";
            string program = System.IO.Path.Combine(Environment.CurrentDirectory, "Ruby\\bin\\ruby.exe");
            if (!File.Exists(program))
                MessageBox.Show("Ruby isn't Installed. Extract Ruby.exe in the same directory as this tool");
            Process p = new Process();
            p.OutputDataReceived +=p_OutputDataReceived;
            p.ErrorDataReceived += p_OutputDataReceived;
            p.StartInfo.FileName = program;
            p.StartInfo.WorkingDirectory = System.IO.Path.Combine(Environment.CurrentDirectory, "Ruby\\bin");
            p.StartInfo.Arguments = MainWindow.customRubyScript;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.UseShellExecute = false;
            
            p.Start();
            p.BeginOutputReadLine();
            while (!p.HasExited)
            {
                //console_output.Focus();
                //console_output.CaretIndex = console_output.Text.Length;
                //console_output.ScrollToEnd();
                DoEvents();
                Thread.Sleep(100);
            }
            show_routes.IsEnabled = true;
            show_routes.Visibility = System.Windows.Visibility.Visible;
            stopRuby.Visibility = System.Windows.Visibility.Hidden;
        }

        private void p_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            string recievedMessage = e.Data;
            
            using (AutoResetEvent errorWaitHandle = new AutoResetEvent(false))
            {
                if (!string.IsNullOrEmpty(recievedMessage))
                {
                    setText(console_output, recievedMessage);
                }
            }
        } 
        void outputConsole(object sender, DataReceivedEventArgs e)
        {
           
        }

        private void setText(TextBox text, string line)
        {           
            try
            {
                this.Dispatcher.Invoke((Action)(() =>
                    {
                        text.Text = string.Format("{0}\n{1}", text.Text, line);
                    }));
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void show_routes_Click(object sender, RoutedEventArgs e)
        {
            //Process.Start(System.IO.Path.Combine(Environment.CurrentDirectory, "Ruby\\bin\\routes"));
            this.Visibility = System.Windows.Visibility.Hidden;
            RouteViewer routeview = new RouteViewer(System.IO.Path.Combine(Environment.CurrentDirectory, "Ruby\\bin\\routes\\"),"*.markdown");
            routeview.Owner = this;
            routeview.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            routeview.ShowDialog();
            this.Visibility = System.Windows.Visibility.Visible;
        }

        private void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void close_button_Click(object sender, RoutedEventArgs e)
        {
            killRuby();
            this.Close();
        }

        private void minimize_button_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }

        private void console_output_TextChanged(object sender, TextChangedEventArgs e)
        {
            console_output.ScrollToEnd();
            DoEvents();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            killRuby();
        }

        private void killRuby()
        {
            foreach (Process p in Process.GetProcessesByName("Ruby"))
            {
                if (!p.HasExited)
                    p.Kill();
            }

        }
    }
}
