﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace EliteDangerousRouteGUI
{
    class Program
    {
        public static bool forceUpdate =false;
        public static string workingDirectory = AppDomain.CurrentDomain.BaseDirectory;
        
        [STAThread]
        static void Main(string[] args)
        {
            Directory.SetCurrentDirectory(workingDirectory);
            EliteDangerousRouteGUI.App app = new EliteDangerousRouteGUI.App();
            if (args.Length > 0 || !File.Exists("autoupdate.exe"))
            {
                if (File.Exists("latest_update.exe"))
                {
                    System.Threading.Thread.Sleep(1000);
                    string argument = string.Format("-o\"{0}\" -y", workingDirectory);
                    ProcessStartInfo pinfo = new ProcessStartInfo("latest_update.exe", argument);
                    pinfo.WorkingDirectory = workingDirectory;
                    pinfo.WindowStyle = ProcessWindowStyle.Hidden;
                    pinfo.CreateNoWindow = true;

                    Process pro = Process.Start(pinfo);
                    while (!pro.HasExited)
                    {
                        System.Threading.Thread.Sleep(100);
                    }
                }
                else
                    forceUpdate = true;
            }

            try
            {                               
                EmbeddedAssembly.Load("EliteDangerousRouteGUI.System.Windows.Controls.Input.Toolkit.dll", "System.Windows.Controls.Input.Toolkit.dll");
                EmbeddedAssembly.Load("EliteDangerousRouteGUI.System.Windows.Controls.Layout.Toolkit.dll", "System.Windows.Controls.Layout.Toolkit.dll");
                EmbeddedAssembly.Load("EliteDangerousRouteGUI.WPFToolkit.dll", "WPFToolkit.dll");
                EmbeddedAssembly.Load("EliteDangerousRouteGUI.Xceed.Wpf.Toolkit.dll", "Xceed.Wpf.Toolkit.dll");
                EmbeddedAssembly.Load("EliteDangerousRouteGUI.Newtonsoft.Json.dll", "Newtonsoft.Json.dll");

                AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

                app.InitializeComponent();
                app.Run();
            }
            catch (Exception ex)
            {
                File.AppendAllText("Error.log", "There was a fatal error using DLL Resources: " + ex.Message);
                return;
            }
        }

        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            return EmbeddedAssembly.Get(args.Name);
        }
    }
}
